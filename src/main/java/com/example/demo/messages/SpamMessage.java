package com.example.demo.messages;

import com.example.demo.SpamAppApplication;
import com.google.gson.Gson;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;

@Component
public class SpamMessage {

    @Autowired
    private final SimpMessagingTemplate template;

    @Autowired
    private Gson gson;

    public SpamMessage(SimpMessagingTemplate template) {
        this.template = template;
    }

    @JmsListener(destination = "spamIncrement", containerFactory = "myFactory")
    public void getSpamIncrement(Message message) throws JMSException {
        ActiveMQTextMessage amqMessage = (ActiveMQTextMessage) message;
        final String messageResponse = amqMessage.getText();
        System.out.println("messageResponse : " + messageResponse);
        final SpamDto spamDto = gson.fromJson(messageResponse, SpamDto.class);
        if (spamDto.isConnected()) {
            SpamAppApplication.connected.increment();
            SpamAppApplication.connected.updateSince(spamDto.getNow());
            if (isSendNotification(SpamAppApplication.connected.getCount())) {
                this.template.convertAndSend("/message", SpamAppApplication.connected.message());
            }
        } else {
            SpamAppApplication.notConnected.increment();
            SpamAppApplication.notConnected.updateSince(spamDto.getNow());
            if (isSendNotification(SpamAppApplication.notConnected.getCount())) {
                this.template.convertAndSend("/message", SpamAppApplication.notConnected.message());
            }
        }
    }

    private boolean isSendNotification(int count) {
        if (count % 10 == 0) {
            return true;
        }
        return false;
    }

}
