package com.example.demo.messages;

public class SpamDto {

    private boolean connected;
    private String now;

    public SpamDto() {
    }

    public boolean isConnected() {
        return connected;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }
}
