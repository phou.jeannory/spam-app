package com.example.demo.utils;

public class NotConnected extends SpamUtil{

    private int count = 0;
    private String since;

    @Override
    public synchronized void increment() {
        count += 1;
    }

    @Override
    public synchronized Integer getCount() {
        return count;
    }

    @Override
    public synchronized void updateSince(final String now) {
        if(count == 1 || since.isEmpty()){
            since = now;
        }
    }

    @Override
    public synchronized String getSince() {
        return since;
    }

    @Override
    public String message() {
        return "les utilisateurs non connectés vous ont spam " + this.getCount() + " fois depuis le "+ this.getSince();
    }

}
