package com.example.demo.utils;

public abstract class SpamUtil {

    public abstract void increment();

    public abstract Integer getCount();

    public abstract void updateSince(String now);

    public abstract String getSince();

    public abstract String message();

}
